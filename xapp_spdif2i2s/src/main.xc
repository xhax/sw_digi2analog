
#include <xs1.h>
#include <platform.h>
#include <print.h>
#include <spdifreceive.h>
#include "i2s_master.h"

#ifndef SAMPLE_BUFF_SIZE
/* Note, buffer index wrap code assumes this is a power of 2 */
#define SAMPLE_BUFF_SIZE 64
#endif

#define SAMPLE_COUNT ((48000/300))

on stdcore[0] : buffered in port:4 p_spdif_rx = PORT_SPDIF_IN;
on stdcore[0] : clock clk_spdif_rx = XS1_CLKBLK_1;

/* Reference clock to external fractional-N clock multiplier */
on tile[1]: out port p_pll_ref    = PORT_PLL_REF;

on stdcore[1] : r_i2s i2sHw =
{
    XS1_CLKBLK_1,
    XS1_CLKBLK_2,
    PORT_MCLK_IN,             // Master Clock
    PORT_I2S_BCLK,            // Bit Clock
    PORT_I2S_LRCLK,           // LR Clock
    {PORT_I2S_ADC0, PORT_I2S_ADC1},
    {PORT_I2S_DAC0, PORT_I2S_DAC1},
};

void AudioHwInit();
void AudioHwConfig(unsigned samFreq, unsigned mClk, chanend ?c_codec, unsigned dsdMode,
    unsigned samRes_DAC, unsigned samRes_ADC);
void genclock();

static const unsigned g_sampRate = 48000;
static const unsigned g_mclk = 48000 * 256;

typedef enum bufferState
{
    BUFF_STATE_UNDERFLOW,
    BUFF_STATE_OVERFLOW,
    BUFF_STATE_NORMAL,
} bufferState_t;

/* Returns 1 for bad parity, else 0 */
static inline int BadParity(unsigned sample)
{
    unsigned X  = (sample >> 4);
    crc32(X, 0, 1);
    return X & 1;
}

#pragma unsafe arrays
int buffer(streaming chanend c_spdif, streaming chanend c_i2s)
{
    unsigned sampleBuffer[SAMPLE_BUFF_SIZE];
    unsigned sample;
    bufferState_t buffState = BUFF_STATE_UNDERFLOW;
    int bufferFill = 0;
    unsigned readPtr = 0;
    unsigned writePtr = 0;
    unsigned refClkVal = 0xffff;
    int sampleCount = 0;

    p_pll_ref <: refClkVal;

    c_i2s <: 0;
    c_i2s <: 0;

    while(1)
    {
        select
        {
            /* Receive an SPDIF sample */
            case c_spdif :> sample:

                if(BadParity(sample))
                {   
                    continue;      // Ignore sample
                }

                sampleCount++;
                if(sampleCount == SAMPLE_COUNT)
                {
                    refClkVal = ~refClkVal;
                    sampleCount = 0;
                    p_pll_ref <: refClkVal;
                }

                if( buffState == BUFF_STATE_OVERFLOW)
                {   
                    continue;      // Ignore sample
                }

                if(((sample & 0xf) == FRAME_X) || ((sample & 0xf) == FRAME_Z))
                {
                    unsigned leftSample = sample << 4;
                    sampleBuffer[writePtr] = leftSample;
                    continue;
                }
                else if((sample & 0xf) == FRAME_Y)
                {
                    /* Store samples */
                    writePtr++;
                    sampleBuffer[writePtr++] = sample << 4;

                    /* Wrap writePtr */
                    writePtr &= (SAMPLE_BUFF_SIZE -1);
                    bufferFill+=2;

                    /* Check if we need to go into overflow or can come out of underflow */
                    if(bufferFill > SAMPLE_BUFF_SIZE-1)
                    {
                        buffState = BUFF_STATE_OVERFLOW;
                    }
                    else if((buffState == BUFF_STATE_UNDERFLOW) && (bufferFill >= (SAMPLE_BUFF_SIZE >>1)))
                    {
                        /* Check if we can come out of underflow */
                        buffState = BUFF_STATE_NORMAL;
                    }
                }
        
            break;

            /* i2s requests a sample */
            case c_i2s :> int _:
                c_i2s :> int _;                

                if(buffState == BUFF_STATE_UNDERFLOW)
                {
                    /* Underflowing - send back 0's */
                    for(int i = 0; i < I2S_MASTER_NUM_CHANS_DAC; i++)
                    {   
                        c_i2s <: 0;
                    }    
                }
                else
                {
                    c_i2s <: sampleBuffer[readPtr++];
                    c_i2s <: sampleBuffer[readPtr++];

                    /* Wrap read pointer */
                    readPtr &= (SAMPLE_BUFF_SIZE - 1);

                    bufferFill-=2;

                    if(bufferFill < 0)
                    {
                        buffState = BUFF_STATE_UNDERFLOW;
                    }

                    if((buffState == BUFF_STATE_OVERFLOW) && (bufferFill < (SAMPLE_BUFF_SIZE >> 1)))
                    {
                        buffState = BUFF_STATE_NORMAL;   
                    } 
                }
            break;
        }
    }
}

int AudioMaster(streaming chanend c_i2s)
{
    unsigned mclkDiv = g_mclk/(g_sampRate * 64);

    AudioHwInit();
    AudioHwConfig(g_sampRate, g_mclk, null, 0, 24, 24);

    /* Run i2s master core */
    i2s_master(i2sHw, c_i2s, mclkDiv);
}


int main()
{
    streaming chan c_spdif, c_i2s;

    par
    {
        on tile[0] : SpdifReceive(p_spdif_rx, c_spdif, 1, clk_spdif_rx);

        on tile[1] : buffer(c_spdif, c_i2s);
        on tile[1] : AudioMaster(c_i2s);
    }

}
