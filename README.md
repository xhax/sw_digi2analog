# xhax/sw_digi2analog

##Description

digi2analog is an opensource application to convert digital streams (e.g. S/PDIF, ADAT) to analogue on the XMOS platform.

Currently the only supported input type is a single sterero S/SPDIF (raw PCM) with output being (stereo) I2S to a DAC.

A fractional-N clock multiplier is required to generate an master-clock for the DAC.

Note, the application is in a very early stage and currently has the following major issues:

    * The application is fixed sample rate, set in main.xc

## Disclaimer

No warranty expressed or implied. The author accepts no responsibilty for any damage to hardware, 
hearing or sanity whilst using this code.

This code is not endorced by XMOS Limited in any shape or form.

## Building

Uses the XMOS standard build system:

```
#!bash

xmake
```

## General Operation

The application runs three cores in total on the XMOS device:
    
    * SpdifReceiver()
    * buffer()
    * AudioMaster()

### SpdifReceive()

This is the low-level hardware driver for SPDIF receive from XMOS module_spdif_receive. 
Please see sc_spdif repo for full documenation.

### buffer ()

The buffer() core implements a small FIFO and generators a reference to an external clock multiplier

### AudioMaster()

This core configures the external audio hardware (i.e. DAC etc) then makes a call to i2s_master().
i2s_master is the low level hardware driver for I2S from module_i2s master
Please see the sc_i2s repo for full documentation.

##TODO

    * Add a hardware support section to this file 
    * Make channel count a build option
    * Calls for detected sample rate
    * Silence detection and indication
    * Auto detect sample rate and dynamically swap
    * Add support for ADAT
    * Add abilty for multiple S/PDIF receivers
    * Add decoding of AC3 etc
    * Level metering?
    * Add SR conversion

## Required software (depandancies)

    * https://github.com/xcore/sc_spdif
    * https://github.com/xcore/sc_i2s
    * https://github.com/xcore/sc_i2c

##Support

Please use the issue tracker
